FROM ruby:2.6.5
RUN apt-get update && apt-get install -y npm
RUN npm install -g yarn
RUN mkdir /alitwitter
WORKDIR /alitwitter
COPY Gemfile Gemfile.lock ./
RUN gem install bundler -v '2.1.4'
RUN bundle install
COPY . ./
RUN yarn
RUN RAILS_ENV=production SECRET_KEY_BASE=secret bundle exec rake assets:precompile
CMD ["bundle", "exec", "rails", "server"]
