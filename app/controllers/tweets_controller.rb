class TweetsController < ApplicationController
  def index
    @tweets = Tweet.order(created_at: :desc).all
  end

  def create
    @tweet = Tweet.new(tweet_params)
    @tweets = Tweet.order(created_at: :desc).all
    if @tweet.save
      redirect_to tweets_url, notice: 'Tweet was successfully created!'
    else
      render 'index'
    end
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    redirect_to tweets_url, notice: 'Tweet was successfully destroyed!'
  end

  private
  def tweet_params
    params.require(:tweet).permit(:message)
  end
end
