# Alitwitter

Twitter like web page

# Deployment

## Dependencies
1. minikube 1.9.0
3. helm 3.1.2

## How to Run

 1. Start minikube
```
minikube start
```

 2. Setup postgresql
```
helm repo add bitnami https://charts.bitnami.com/bitnami

helm install alitwitter-postgres -f pg-values.yml bitnami/postgresql
```

 3. Setup application
```
helm install alitwitterchart alitwitterchart
```

4. Opening app
Open from browser 
```
<minikube_ip>:30000/tweets
```
