require 'test_helper'

class TweetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tweet = tweets(:one)
  end

  test 'should get index' do
    get tweets_url
    assert_response :success
  end

  test 'should create tweet' do
    assert_difference('Tweet.count') do
      post tweets_url, params: { tweet: { message: @tweet.message } }
    end

    assert_redirected_to tweets_url
  end

  test 'should destroy tweet' do
    assert_difference('Tweet.count', -1) do
      delete tweet_url(@tweet)
    end

    assert_redirected_to tweets_url
  end
end
