require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  test 'creating a tweet with blank message' do
    tweet = Tweet.new(message: ' ')
    assert_not tweet.save
  end

  test 'should not save tweet with 141 characters in message' do
    tweet_message = 'Lorem Ipsum is simply dummy text of' \
    'the printing and typesetting industry.' \
    'Lorem Ipsum has been the industry\'s standard dummy' \
    'text ever since the 1500s, when an unknown printer' \
    'took a galley of type and scrambled it to make a type' \
    'specimen book. It has survived not only five centuries,' \
    'but also the leap into electronic typesetting,' \
    'remaining essentially unchanged.'

    tweet = Tweet.new(message: tweet_message)
    assert_not tweet.save
  end
end
